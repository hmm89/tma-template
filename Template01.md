\documentclass{article}
\author{\textbf{Your Name} 0123456789}
\title{TMA Template Question 1}
\usepackage{graphicx}
\parskip 4pt
\parindent 0pt
\linespread{1.3}
\usepackage{amsmath} 
\usepackage{wrapfig}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{fancyhdr}
\usepackage{lastpage}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,RO]{TMA Template}
\fancyhead[RE,LO]{\textbf{Your Name} E012345678}
\rfoot{Page \thepage \hspace{1pt} of \pageref{LastPage}}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{2pt}

\begin{document}
\begin{center}
\textbf{TMA Template \\ Question 1}\\
\end{center}

\newpage
\begin{center}
\textbf{Question 2}\\
\end{center}

\newpage
\begin{center}
\textbf{Question 3}\\
\end{center}

\newpage
\begin{center}
\textbf{Question 4}\\
\end{center}

\end{document}